<footer class="main-footer">
		<div class="container">
			<div class="f_left">
				<p>&copy; <?php echo date("Y"); ?> - Advanced WP Theme</p>
			</div>
			<div class="f_right">
				<?php
					$args = array(
						'theme_location' => 'footer'
					);
					wp_nav_menu($args);
				?>
				</div>
		</div>
  </footer>
  <?php wp_footer(); ?>
</body>
</html>