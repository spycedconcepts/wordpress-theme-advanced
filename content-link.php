<article class="post post-link">
  <div class="well">
    <a href="<?php echo get_the_content(); ?>" target="_blank">
      <?php echo get_the_title(); ?>
    </a>
  </div>
</article>