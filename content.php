<?php if(is_search()  || is_archive()) :  ?>

<div class="archive-post">
  <h4>
    <a href="<?php the_permalink(); ?>">
    <?php the_title(); ?>
  </a>
  </h4>
  <p>Posted on : <?php the_time('F j, Y g:i a') ?></p>
</div>

<?php else : ?>
  <article class="post">
    <h3>
    <a href="<?php the_permalink(); ?>">
      <?php the_title(); ?>
    </a>
    </h3>
    <div class="meta">
    Created By 
    <a href="<?php get_author_posts_url(get_the_author_meta('ID')); ?>">
      <?php the_author(); ?> 
    </a>
    on <?php the_time('F j, Y g:i a'); ?>
    </div>
    <?php if(has_post_thumbnail()) : ?>
      <div class="post-thumbnail">
        <?php the_post_thumbnail(); ?>
      </div>
    <?php endif; ?>
    <?php if(is_single()) : ?>
      <?php the_content(); ?>
    <?php else : ?>
    <?php the_excerpt(); ?>
      <br>
      <a class="button" href="<?php the_permalink(); ?>">
        Read More
      </a>
    <?php endif; ?>
  </article>
<?php endif; ?>