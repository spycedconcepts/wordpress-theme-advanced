<h2> Comments </h2>
<?php $args = array(
  'walker' => null,
  'max_depth' => '',
  'style' => 'ul',
  'callback' => null,
  'end-callback' => null,
  'type' => 'all',
  'reply_text' => 'Reply',
  'page' => '',
  'per_page' => '',
  'avatar_size' => '80',
  'reverse_top_level' => null,
  'reverse_children' => '',
  'format' => 'html5',
  'short_ping' => false,
  'echo' -> true
); ?>

<?php wp_list_comments($args, $comments); ?>

<?php $comments_args = array (
  'label_submit' => 'Send',
  'title_reply' => 'Write a reply or comment...',
  'comment_notes_after' => '',
  'comment_field' => 
    '<p class="comment-form-content"><label for="comment">' . 
    _x('Comment', 'noun') . 
    '</label><br /> <textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>'
); ?>

<?php comment_form($comment_args); ?>